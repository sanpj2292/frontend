var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var beautify_html = require('js-beautify').html_beautify;

router.get('/', function(req,res) {
    res.send('My Application is running alright!!')
});

router.route('/insertComponent').post((req,res) => {    
    var cnt = req.body.comp_cnt;
    var cmp_nm = req.body.cmp_nm;
    switch (req.body.compType) {
        case 'Submit Button':
            html = 
            `<div class="form-group row">
                <div class="col-sm-5 offset-sm-2">
                    <button type="submit" name="${cmp_nm}" class="btn btn-primary sub-btn">Submit${cnt}</button>
                </div>
            </div>`;
            break;
        case 'Text Input':
            html = 
            `<div class="form-group row">
                <label for="input${cnt}" class="col-sm-2 col-form-label">Input ${cnt} </label>
                <div class="col-sm-5">
                    <input type="input" name="${cmp_nm}" class="form-control" id="input${cnt}" placeholder="Input ${cnt}">
                </div>
            </div>`;
            break;
        case 'Radio button':
               html= `<div class="form-group row">
                    <label class="col-sm-5">
                        <input type="radio" class="mr-1" name="${cmp_nm}"> Radio${cnt}
                    </label>
                </div>`
            break;
        case 'Checkbox':
            html= `<div class="form-group row">
                    <div class="col-sm-5 offset-sm-2">
                        <div class="checkbox">
                            <label class="chk-lbl"><input name="${cmp_nm}" type="checkbox"> Checkbox${cnt}</label>
                        </div>
                    </div>
                </div>`
            break;
        case 'File Input':
            html = 
            `<div class="form-group row">
                <label for="fileinput${cnt}" class="col-sm-2 col-form-label"> FileInput ${cnt} </label>
                <div class="col-sm-10">
                    <input type="input" name="${cmp_nm}" class="form-control" id="fileinput${cnt}" placeholder="FileInput ${cnt}">
                </div>
            </div>`;
            break;
    }
    res.send(html);
});

/**
 * This function makes a html string from the dragged component list
 * @param {array:string} compList - contains html strings of dragged elements
 * @returns {string} a string containing all the dragged html elements
 */
var _makeCompHtmlString = function(compList) {
    var compHtmlString = '';
    compList.forEach(element => {
        compHtmlString = compHtmlString + element + '\n';
    });
    return compHtmlString
};

router.route('/writeHtml').post((req,resp,nxt) => {
    console.log(req.body);
    let html_content = _makeCompHtmlString(req.body.cmp_arr);
    var temp = 
        `<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="utf-8" />
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="manifest" href="%PUBLIC_URL%/manifest.json" />
        <title>Generated Template</title>
    </head>
    <body>
        <div id="gen-root">
            ${html_content}
        </div>
    </body>
    </html>`
    var fpath = path.resolve(__dirname+'../../../write-1.html')
    fs.writeFile(fpath, beautify_html(temp), function( err , buff) {
        if (err) throw nxt(err);
        console.log(`Html file is present in ${fpath}`);
        console.log('Writing to html file is done!!');
        resp.end();
    });
});

// TODO: Attachment file name parameter to obtain from request
router.route('/download').get((req,resp) => {
    var filename = path.resolve(__dirname+'..\\..\\..\\write-1.html');
    fs.readFile(filename, (e,content) => {
        if (e) throw e;
        resp.setHeader('Content-disposition', 'attachment; filename=index.html');
        resp.end(content);
    });
});



module.exports = router;