var express = require('express');
var path = require('path');
var app = express();
var bodyParser= require('body-parser');
var router = require('./routes/routes.js');
var cors = require('cors');
const port = 4000;


// app.use(express.static(path.join(__dirname, '../public')));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
app.use(cors());

app.use('/', router);

module.exports=app;