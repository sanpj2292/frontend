// This server is used to show the generated html document

var express = require('express');
var path = require('path');
var app = express();
var bodyParser= require('body-parser');
var cors = require('cors');
var fs = require('fs');
const port = 8081;


app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false}));

app.listen(port, () => console.log(`Html Generator Server listening on port ${port}!`))
app.use(cors());

app.use('/', (req, resp) => {
    var filename = path.resolve(__dirname+'..\\..\\write-1.html');
    fs.readFile(filename, (e,content) => {
        if (e) throw e;
        resp.setHeader('Content-disposition', 'attachment; filename=index.html');
        resp.end(content);
    });
});

module.exports = app;