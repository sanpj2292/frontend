# Dynamic HTML Generator

A Dynamic HTML Generator application

This application has been developed Using ReactJs, NodeJs with Express

In this the dependent packages are 
1) react-bootstrap
2) path
3) bodyparser
4) express
5) create-react-app
6) react-html-parser
7) fs
8) cors
9) util
10) querystring
11) axios

You can install above using `npm i xxx yyyy zzzzz -s`

The backend support is done by NodeJs using Express framework

The Backend acts as API for the front-end, to avoid Cross-origin restriction we make use of cors module
The Frontend is completed using ReactJs & react-bootstrap(for css)

Both Front-end & back-end servers run on same host and different ports

Front-end Port: 3000
Back-end Port: 4000

##### Instructions:
Before you start running the application, check if all the modules are installed or not

To run backend server, we will need to navigate to `server` folder and run `node server.js`

To run frontend server, we would need to navigate to `frontend` folder(the project folder) & simply run `npm start`

In this application, implementation has been done only for 5 components File Input, Text Input, Submit Button, Checkbox & Radio Button

The drag & drop needs to be done in the centre div which has no components in it at all(for the first time)

##### Features:
1) You can drag & drop into the dropping `div` & also look at the approximate layout
2) The size, margins are fixed one!
3) There is a `HTML Output` button at the top-right corner which would produce a file `write-1.html` in the project folder
4) The file `write-1.html` contains the generated output of aligned HTML
5) User will be asked to input `name` attribute of each component, if left empty a default name would be used for the component


Happy Generating :P!!
