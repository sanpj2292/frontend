import React from 'react';
import {Button} from 'react-bootstrap';
import './css/sidemenu.css';

// Components supported in Dynamic Html generator
const comps = ['Text Input', 'Radio button', 'Checkbox', 'File Input', 'Submit Button'];

class Sidemenu extends React.Component {

    render() {
        return (
         <div>
            {comps.map(comp => {
                return <Button draggable key={comp} className='btn-lg btn-block pad-sm'
                onDragStart={(e) => {
                    console.log('onDragStart Event for Sidemenu');
                    e.dataTransfer.setData('text/plain',e.target.innerText);
                  }}
                
                > {comp} </Button>
            })}
        </div>
        )
    }
}

export default Sidemenu;