import React from 'react';
import './App.css';
import axios from 'axios';
import  querystring from 'querystring';
import {Form, Navbar, Col, Row, Button} from 'react-bootstrap';
import Sidemenu from './sidemenu';
import ReactHtmlParser from 'react-html-parser';
import { isUndefined } from 'util';
// TODO: Add Dialogs wherever necessary
// TODO: Add Toasters for receiving Completion status
// TODO: Add Dialogs for Error Status
// TODO: Add Log Files as well, to assist in error management
class App extends React.Component {

  constructor() {
    super();
    // Configure API URL into axios
    axios.defaults.baseURL = 'http://localhost:4000';
    this.state = {
        comp_cnt: 0,
        inp_cnt: 0,
        rd_cnt: 0,
        chk_cnt: 0,
        fin_cnt: 0,
        sub_cnt: 0,
        dragged_comps: [],
        html_output: false,
        gen_html: ''
    }
  }


  updateCount(type) {
    var someProperty = {...this.state};
    var cnt = 0;
    switch(type) {
        case 'Text Input':
            someProperty.inp_cnt = this.state.inp_cnt + 1;
            cnt = someProperty.inp_cnt;
            break;
        case 'Radio button':
            someProperty.rd_cnt= this.state.rd_cnt + 1;
            cnt = someProperty.rd_cnt;
            break;
        case 'Checkbox':
            someProperty.chk_cnt = this.state.chk_cnt + 1;
            cnt = someProperty.chk_cnt;
            break;
        case 'Submit Button':
            someProperty.sub_cnt = this.state.sub_cnt + 1;
            cnt = someProperty.sub_cnt;
            break;
        case 'File Input':
            someProperty.fin_cnt = this.state.fin_cnt + 1;
            cnt = someProperty.fin_cnt;
            break;
        }
        this.setState(someProperty);
        return cnt;
}
  
  resetState() {
    return {
        comp_cnt: 0,
        inp_cnt: 0,
        rd_cnt: 0,
        chk_cnt: 0,
        fin_cnt: 0,
        sub_cnt: 0,
        dragged_comps: [],
        html_output: false,
        gen_html: ''
    };
  }

  executeDrop(cmpType) {
    if (cmpType && cmpType !== ''){
      var up_cnt = this.updateCount(cmpType);
      var cmp_nm = prompt('Enter the name of the component: ');
      // If empty -> A default value will be taken
      cmp_nm = (isUndefined(cmp_nm) || cmp_nm.length <= 0) ? cmpType+up_cnt : cmp_nm;
  
      // Configure API URL into axios
      axios.post('/insertComponent',
          querystring.stringify(
              {
                compType: cmpType,
                cmp_nm: cmp_nm,
                comp_cnt: up_cnt
              })
          ).then((resp) => {
              var state = {...this.state};
              state.dragged_comps.push(resp.data);
              this.setState(state);
          })
    }
  }

  generateHtmlOutput() {
    axios.post('/writeHtml', { cmp_arr: this.state.dragged_comps}).then(
      (resp) => {
          var state = {...this.state};
          state.html_output = true;
          state.gen_html = resp.data;
          this.setState(state);
        }).catch((error) => {
          console.error('Something Went wrong!');
          throw error;
        });
    }

  

  show_download_html() {
    // TODO: Add Download File-name provision feature
    window.open('http://localhost:4000/download', '_blank');
  }

  render() {
    const enabled = this.state.dragged_comps && this.state.dragged_comps.length > 0;
    const html_output = this.state.html_output;
    return (
      <div className="App"
        onDragOver={even =>even.preventDefault()}
        onDrop={(event) => {
          let comp_type = event.dataTransfer.getData('text/plain');
          if(event.target.classList.contains('droppable')){
            this.executeDrop(comp_type);
          }
        }}
      >
        <Navbar bg="dark" variant="light" expand="md">
          <Navbar.Brand > Web Interface </Navbar.Brand>

          <Button className={'pull-right btn-va link-color btn-outline-light'}
            variant="link"
            disabled={!html_output}
            onClick={()=>this.show_download_html()}
            >HTML Output</Button>

          <Button className={'pull-right btn-va btn-outline-success'} 
              variant="light"
              disabled={!enabled}
              onClick={()=>this.generateHtmlOutput()}
              >Generate</Button>
          
        </Navbar>
        <Form>
          <Row >
            <Col className='overflow-auto h-100 col-sm-2 bg-light'>
              <Sidemenu />
            </Col>
            <Col className='overflow-auto col-sm-10 h-100 bg-light comp-area-height-full droppable'>
              <div >
                {this.state.dragged_comps.map(com => ReactHtmlParser(com))}
              </div>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
  
}

export default App;
